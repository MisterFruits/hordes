# coding: utf-8
"""Basic unit tests for coco package"""
from coco import cout_total_en_pa, csv_en_batiment, devis

def test_cout_total_en_pa():
    batiment = {'planche': 4, 'poutre': 0, 'ferraille': 0, 'structure': 6, 'pa': 50}
    assert cout_total_en_pa(batiment, scie=True, manuf=True) == 66
    assert cout_total_en_pa(batiment, scie=False, manuf=True) == 82
    assert cout_total_en_pa(batiment, scie=True, manuf=False) == 82
    assert cout_total_en_pa(batiment, scie=False, manuf=False) == 98

def test_csv_en_batiment():
    assert {'erf': 6, 'tgf': 9} == {'tgf': 9, 'erf': 6}
    with open('tests/resources/exemple_batiements.csv', 'r') as csv:
        batiments = csv_en_batiment(csv)
        expected = [{'nom': 'jardin des plantes', 'pa': 100, 'planche': 3, 'ferraille' :3, 'poutre': 0, 'structure': 0},
                    {'nom': 'avion', 'pa': 90, 'planche': 1, 'ferraille' :10, 'poutre': 0, 'structure': 100},
                    {'nom': 'voiture', 'pa': 50, 'planche': 0, 'ferraille' :3, 'poutre': 0, 'structure': 10}]
        sort_by_name = lambda x: x['nom']
        batiments.sort(key=sort_by_name)
        expected.sort(key=sort_by_name)

        assert batiments == expected
        for expected_bat in expected:
            keys = expected_bat.keys()
            extract = [{key: bat.get(key, None) for key in keys} for bat in batiments]
            assert expected_bat in extract

def test_devis():
    with open('tests/resources/exemple_batiements.csv', 'r') as csv:
        le_devis = devis(csv_en_batiment(csv), True, True)
        expected = {
              "scie": True,
              "manufacture": True,
              "couts_de_base": [1, 2],
              "batiments": [{'nom': 'jardin des plantes', 'pa': 100, 'planche': 3, 'ferraille' :3, 'poutre': 0, 'structure': 0, 'total_pa': 106},
                            {'nom': 'avion', 'pa': 90, 'planche': 1, 'ferraille' :10, 'poutre': 0, 'structure': 100, 'total_pa': 301},
                            {'nom': 'voiture', 'pa': 50, 'planche': 0, 'ferraille' :3, 'poutre': 0, 'structure': 10, 'total_pa': 73}],
            }
        assert le_devis == expected
