# coding: utf-8
"""Basic integration tests for hordes scripts"""

def test_initialization():
    """Check the test suite runs by affirming 2+2=4"""
    assert 2 + 2 == 4

def test_import_coco():
    """Ensure the test suite can import our module"""
    try:
        import coco
    except ImportError:
        assert False, "Was not able to import the tabtranslator module"
