Ceci est un document de spec tant que le projet sera en phase de developpement.

# Chantiers
C'est le script `coco.py` qui va assurer cette feature.

## Status
Affiche une liste des chantiers avec
 - le cout en PA pour transformer (depuis souche/debris ou plancher/fer ou ressources actuelle) + PA
 - ratio défense/PA défense/PVE défense/ressources de base

## Devis
permet de sélectionner plusieurs chantier et sortir un rapport du cout de ces chantiers
Sera implémenenté par les fonction tri et filtre

## Les entrées
 - Scie: oui/non
 - manuf: oui/non (peut-être détecté via les chantiers construits ?)

## les options
**affiche ou non les ratio**

**affiche cout total**
 - choix: oui/non
 - implémentation: simple booléen

**format de sortie**:
 - choix: cli / hordes / html
 - implémentation: liste de choix + template ?

**filtre**:
 - choix: 1 liste types de filtres listant eux même plusieurs options
 - type "max ressources": utilise max pve/eau/...
 - type "selection par le nom" seulement batiments nommé (= mode devis)
 - implémentation: sur la liste totale des résultat appliquer un filtre avant de les passer à l'afficheur

**tri**:
choix
 - part PA
 - ressources
 - défense

## TODO
 - Récupération dynamiques des ressources, chantiers construits

# Prévision des ouvirers
Calculs PA dispos, envisage possibilité expé

**example**: "Ce qui fait qu'il nous reste encore 35 (vivants) - 6 (dehors) - 5 (expé) - 4 (marge de 10% d'inactif) = //20 batisseurs / FA//"
