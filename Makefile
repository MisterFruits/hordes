help: # List of the targets
	@grep -E '[a-z0-9]+:' Makefile

test: # Launch the test suite
	coverage run -m py.test

cov: test # Display coverage statistics and create an HTML report
	coverage report
	coverage html

doc:
	make -C documentation html
