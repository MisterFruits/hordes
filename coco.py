# coding: utf-8
import jinja2
"""Simulateur de chantiers pour hordes"""

def cout_total_en_pa(batiment, scie=False, manuf=False):
    nb_transfo = (batiment['planche'] + batiment['ferraille'] +
        (batiment['poutre'] + batiment['structure']) * 2)

    return batiment['pa'] + nb_transfo * cout_transfo_atelier(scie, manuf)

def cout_transfo_atelier(scie=False, manuf=False):
    facteur_transfo = 3
    if scie:
        facteur_transfo -= 1
    if manuf:
        facteur_transfo -= 1
    return facteur_transfo

def csv_en_batiment(csv_lines):
    result = []
    head, *csv_bat = csv_lines
    head = [el.strip().lower() for el in head.split(',')]
    for line in csv_bat:
        batiment = dict(zip(head, [el.strip().lower() for el in line.split(',')]))
        for key, item in batiment.items():
            try:
                batiment[key] = int(item)
            except ValueError:
                if item == '':
                    batiment[key] = 0

        result.append(batiment)

    return result

def devis(batiments, scie, manuf):
    facteur_transfo = cout_transfo_atelier(scie, manuf)
    columns = [lambda x: ('total_pa', cout_total_en_pa(x, scie, manuf))]
    for bat in batiments:
        add_column(bat, *columns)
    return {
              "scie": scie,
              "manufacture": manuf,
              "couts_de_base": [facteur_transfo * el for el in [1, 2]],
              "batiments": batiments,
            }

def add_column(batiment, *columns):
    for column in columns:
        column_name, value = column(batiment)
        batiment[column_name] = value


def main():
    import argparse
    ap = argparse.ArgumentParser(description=__doc__)
    ap.add_argument('--scie', '-s', help="Activer l'utilisation de la scie",
                     action='store_true', default=False)
    ap.add_argument('--manuf', '-m', help="Activer l'utilisation de la manufacture",
                     action='store_true', default=False)
    args = ap.parse_args()

    env = jinja2.Environment(loader=jinja2.PackageLoader('contrib', 'templates'))
    cli = env.get_template('devis.cli')

    with open('contrib/s14_bat.csv', 'r') as csv:
        print(cli.render(**devis(csv_en_batiment(csv), args.scie, args.manuf)))

if __name__ == '__main__':
    main()
